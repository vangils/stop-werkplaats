# STOP-Werkplaats

Repository ter ondersteuning van werksessies voor het ontwikkelen van realistische voorbeeldbestanden voor de STOP TPOD Standaard.

## Hoe maak ik een voorbeeldbestand

Het doel van de werksessies is het maken van realistische voorbeeldbestanden die
voldoen aan de STOP/TPOD standaarden. Hier werken we naartoe in een aantal
stappen:

- Maak een versie van een regeling (dus nog geen besluit) dat voldoet aan het imop.xsd.
- Verrijk dit voorbeeldbestand met identifiers.
- Maak (Geo-)Datacollecties die je wilt koppelen aan het besluit
- Koppel tekst en data waardoor informatieobjecten en datacollecties ontstaan.
- Voeg kenmerken toevoegen (maakt tekst machine-interpreteerbaar)
- Voeg werkingsgebieden toe
- XML fragment inbedden in Besluit (+ muteren)
- Metadata toevoegen

## Deelnemende praktijkproeven

- Verordening Provincie Zuid-Holland
- Verordening Provincie Gelderland en daarop volgend Mutatiebesluit
- Tien meldingen waterschap Drents Overijsselse Delta
- Ministeriele regeling
- Omgevingsvisie Rotterdam (in voorbereiding)
- Projectbesluit Zuidas (in voorbereiding)
- Casus Delfland (geen praktijproef maar wel belangrijk)

